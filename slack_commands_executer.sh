#!/bin/bash
set -euo pipefail
FAILURE=1
SUCCESS=0
SLACKWEBHOOKURL="https://hooks.slack.com/services/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"
todayDate=`date`
reportedTest="Test"
subReportfolder="20220627_151854"
subTestSuiteReportfolder="20220627_151854"
function print_slack_summary_build() {
	local slack_msg_header
	local slack_msg_body
	local slack_channel
	# Populate header and define slack channels
	slack_msg_header=":heavy_check_mark: *Run Report*"
	cat <<-SLACK
	{
		"blocks": [
			{
				"type": "divider"
			},
			{
				"type": "header",
				"text": {
					"type": "plain_text",
					"text": ":books:  Test Case Generated Reports  :books:"
				}
			},
			{
				"type": "context",
				"elements": [
					{
						"text": "*:Date: $todayDate*  |  :book:$reportedTest",
						"type": "mrkdwn"
					}
				]
			},
			{
				"type": "divider"
			},
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "*Report Availabe Extentions*"
				}
			},
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "*Download HTML version of the report*"
				},
				"accessory": {
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "HTML",
						"emoji": true
					},
					"value": "html_report",
					"url": "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${CI_JOB_ID}/artifacts/file/testDockerImage/Reports/${subReportfolder}/testSuite1/${subTestSuiteReportfolder}/${subTestSuiteReportfolder}.html",
					"action_id": "button-action"
				}
			},
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "*Download Logs version*"
				},
				"accessory": {
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Logs",
						"emoji": true
					},
					"value": "logs",
					"url": "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${CI_JOB_ID}/artifacts/file/testDockerImage/Reports/${subReportfolder}/testSuite1/${subTestSuiteReportfolder}/execution0.log",
					"action_id": "button-action"
				}
			},
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "*Download CSV version of the report*"
				},
				"accessory": {
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "CSV",
						"emoji": true
					},
					"value": "csv_report",
					"url": "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${CI_JOB_ID}/artifacts/file/testDockerImage/Reports/${subReportfolder}/testSuite1/${subTestSuiteReportfolder}/${subTestSuiteReportfolder}.csv",
					"action_id": "button-action"
				}
			},
			{
				"type": "divider"
			}
		]
	}
	SLACK
}
function share_slack_update_build() {
local slack_webhook
slack_webhook="$SLACKWEBHOOKURL"
curl -X POST                                           \
        --data-urlencode "payload=$(print_slack_summary_build)"  \
        "${slack_webhook}"
}
share_slack_update_build
